<?php
require_once("../utils/db_connect.php");

//require("mailer.php");
if ($_SERVER['REQUEST_METHOD'] == "POST") $method = $_POST;
else $method = $_GET;


$valid = true;
if (count($_POST)) {
    $keys = ['firstname', 'lastname', 'email', 'birthdate', 'num', 'street', 'postal', 'city', 'login', 'pwd'];
    foreach ($keys as $key) {
        if (!isset($_POST[$key]) && trim($_POST[$key]) == '') $valid = false;
    }
} else $valid = false;

if (!$valid) {
    echo json_encode(['success' => false, 'msg' => "Toutes les valeurs n'ont pas été transmises"]);
    die();
}

$pwd = password_hash($_POST['password'], PASSWORD_DEFAULT);

$sql = "INSERT INTO users (firstname_user, lastname_user, email_user, birthdate_user,num_street,street,postal_code,city, login, password) VALUES ('{$method['firstname']}', '{$method['lastname']}', '{$method['email']}', '{$method['birthdate']}', {$method['num']}, '{$method['street']}', {$method['postal']}, '{$method['city']}', '{$method['login']}', '{$pwd}')";
$db->query($sql);

echo json_encode(['success' => true]);



/*
mailer($_POST['email'], 'Bienvenue', "Merci pour votre inscription");
//Mailer
*/



