$("input:submit").click((e) => {
    e.preventDefault();

    const firstname = $('#firstname_user').val();
    const lastname = $('#lastname_user').val();
    const email = $('#email_user').val();
    const birthdate = $('#birthdate_user').val();
    const num = $('#num_street').val();
    const street = $('#street').val();
    const postal = $('#postal_code').val();
    const city = $('#city').val();
    const login = $('#login').val();
    const pwd = $('#password').val();

    $.ajax({
        url: "register.php",
        type: "POST",
        data: {
            firstname,
            lastname,
            email,
            birthdate,
            num,
            street,
            postal,
            city,
            login,
            pwd
        },
        dataType: "json",
        success: (res, status) => {
            if (res.success) {
                alert("tout est bon");
            } else alert(res.msg);
        }
    })
})